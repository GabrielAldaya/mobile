﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(ChunkEditorScript))]
public class ChunkEditorData : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        ChunkEditorScript myScript = (ChunkEditorScript)target;

        if (GUILayout.Button("Save"))
        {
            myScript.SaveData();
        }

        #region CREATE
        if (GUILayout.Button("Create Trap1"))
        {
            myScript.CreateTrap1();
        }
        if (GUILayout.Button("Create Trap2"))
        {
            myScript.CreateTrap2();
        }
        if (GUILayout.Button("Create Trap3"))
        {
            myScript.CreateTrap3();
        }
        if (GUILayout.Button("Create Switch1"))
        {
            myScript.CreateSwitch1();
        }
        if (GUILayout.Button("Create Switch2"))
        {
            myScript.CreateSwitch2();
        }

        if (GUILayout.Button("Create Coin1"))
        {
            myScript.CreateCoin1();
        }
        if (GUILayout.Button("Create Coin2"))
        {
            myScript.CreateCoin2();
        }

        #endregion

        #region DELETE
        if (GUILayout.Button("Delete Last Trap1"))
        {
            myScript.DeleteTrap1();
        }
        if (GUILayout.Button("Delete Last Trap2"))
        {
            myScript.DeleteTrap2();
        }
        if (GUILayout.Button("Delete Last Trap3"))
        {
            myScript.DeleteTrap3();
        }
        if (GUILayout.Button("Delete Last Switch1"))
        {
            myScript.DeleteSwitch1();
        }
        if (GUILayout.Button("Delete Last Switch2"))
        {
            myScript.DeleteSwitch2();
        }
        if (GUILayout.Button("Delete Last Coin1"))
        {
            myScript.DeleteCoin1();
        }
        if (GUILayout.Button("Delete Last Coin2"))
        {
            myScript.DeleteCoin2();
        }
        #endregion
    }
}
#endif