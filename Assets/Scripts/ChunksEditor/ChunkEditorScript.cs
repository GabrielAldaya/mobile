﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Collections.Generic;

public class ChunkEditorScript : MonoBehaviour {

    private List<GameObject> Childs = new List<GameObject>();
    [SerializeField]
    List<GameObject> Traps1 = new List<GameObject>();
    [SerializeField]
    List<GameObject> Traps2 = new List<GameObject>();
    [SerializeField]
    List<GameObject> Traps3 = new List<GameObject>();
    [SerializeField]
    List<GameObject> Switches1 = new List<GameObject>();
    [SerializeField]
    List<GameObject> Switches2 = new List<GameObject>();
    [SerializeField]
    List<GameObject> Coins = new List<GameObject>();
    [SerializeField]
    List<GameObject> Coins2 = new List<GameObject>();

    [SerializeField]
    private int ChildsAmount = 0;
    //
    [Header("txt Name File on Project Folder")]
    [SerializeField]
    string fileName;
    //
    StreamWriter sr;
    GameObject PrefabTrap1;
    GameObject PrefabTrap2;
    GameObject PrefabTrap3;
    GameObject PrefabSwitch1;
    GameObject PrefabSwitch2;
    GameObject PrefabCoin;
    GameObject PrefabCoin2;
    int traps1count = 0;
    int traps2count = 0;
    int traps3count = 0;
    int switches1count = 0;
    int switches2count = 0;
    int coinscount = 0;
    int coins2count = 0;

    void Start()
    {
        fileName = "ChunkEditorData.txt";
        PrefabTrap1 = Resources.Load<GameObject>("Prefabs/Gameplay/Traps/trap");
        PrefabTrap2 = Resources.Load<GameObject>("Prefabs/Gameplay/Traps/trap2");
        PrefabTrap3 = Resources.Load<GameObject>("Prefabs/Gameplay/Traps/trap3");
        PrefabSwitch1 = Resources.Load<GameObject>("Prefabs/Gameplay/Traps/switch");
        PrefabSwitch2 = Resources.Load<GameObject>("Prefabs/Gameplay/Traps/switch2");
        PrefabCoin = Resources.Load<GameObject>("Prefabs/Gameplay/Coins/coin");
        PrefabCoin2 = Resources.Load<GameObject>("Prefabs/Gameplay/Coins/coin2");

    }

    void FixedUpdate()
    {
        if (ChildsAmount != Childs.Count)
        {
            ChildsAmount = Childs.Count;
        }
    }

    public void CreateTrap1()
    {
        GameObject go = Instantiate(PrefabTrap1);
        go.transform.SetParent(gameObject.transform);
        go.name = "Trap1 " + traps1count;
        traps1count++;
        Traps1.Add(go);
    }

    public void CreateTrap2()
    {
        GameObject go = Instantiate(PrefabTrap2);
        go.transform.SetParent(gameObject.transform);
        go.name = "Trap2 " + traps2count;
        traps2count++;
        Traps2.Add(go);
    }

    public void CreateTrap3()
    {
        GameObject go = Instantiate(PrefabTrap3);
        go.transform.SetParent(gameObject.transform);
        go.name = "Trap3 " + traps3count;
        traps3count++;
        Traps3.Add(go);
    }

    public void CreateSwitch1()
    {
        GameObject go = Instantiate(PrefabSwitch1);
        go.transform.SetParent(gameObject.transform);
        go.name = "Switch1 " + switches1count;
        switches1count++;
        Switches1.Add(go);
    }

    public void CreateSwitch2()
    {
        GameObject go = Instantiate(PrefabSwitch2);
        go.transform.SetParent(gameObject.transform);
        go.name = "Switch2 " + switches2count;
        switches2count++;
        Switches2.Add(go);
    }

    public void CreateCoin1()
    {
        GameObject go = Instantiate(PrefabCoin);
        go.transform.SetParent(gameObject.transform);
        go.name = "Coin1 " + coinscount;
        coinscount++;
        Coins.Add(go);
    }

    public void CreateCoin2()
    {
        GameObject go = Instantiate(PrefabCoin2);
        go.transform.SetParent(gameObject.transform);
        go.name = "Coin2 " + coins2count;
        coins2count++;
        Coins2.Add(go);
    }

    public void DeleteTrap1()
    {
        GameObject temp = Traps1[Traps1.Count-1];
        Traps1.RemoveAt(Traps1.Count-1);
        Destroy(temp);
        traps1count--;
    }

    public void DeleteTrap2()
    {
        GameObject temp = Traps2[Traps2.Count - 1];
        Traps2.RemoveAt(Traps2.Count - 1);
        Destroy(temp);
        traps2count--;
    }

    public void DeleteTrap3()
    {
        GameObject temp = Traps3[Traps3.Count - 1];
        Traps3.RemoveAt(Traps3.Count - 1);
        Destroy(temp);
        traps3count--;
    }

    public void DeleteSwitch1()
    {
        GameObject temp = Switches1[Switches1.Count - 1];
        Switches1.RemoveAt(Switches1.Count - 1);
        Destroy(temp);
       switches1count--;
    }

    public void DeleteSwitch2()
    {
        GameObject temp = Switches2[Switches2.Count - 1];
        Switches2.RemoveAt(Switches2.Count - 1);
        Destroy(temp);
        switches2count--;
    }

    public void DeleteCoin1()
    {
        GameObject temp = Coins[Coins.Count - 1];
        Coins.RemoveAt(Coins.Count-1);
        Destroy(temp);
        coinscount--;
    }

    public void DeleteCoin2()
    {
        GameObject temp = Coins2[Coins2.Count - 1];
        Coins2.RemoveAt(Coins2.Count - 1);
        Destroy(temp);
        coins2count--;
    }

    public void SaveData()
    {
        Traps1.AddRange(Traps2);
        Traps1.AddRange(Traps3);
        Traps1.AddRange(Switches1);
        Traps1.AddRange(Switches2);
        Traps1.AddRange(Coins);
        Traps1.AddRange(Coins2);
        Childs.AddRange(Traps1);
        if (Childs.Count==0)
        {
            Debug.Log("There Must be at Least One Object");
            return;
        }
        if (File.Exists(fileName))
        {
            sr = File.AppendText(fileName);
            sr.WriteLine("\"id\": X,");
            sr.WriteLine("\"objects\": [{");

            for (int i = 0; i < Childs.Count; i++)
            {
                if (Childs[i].tag == "trap")
                {
                    if (Childs[i].name.Substring(4, 1) == "1")
                    {
                        sr.WriteLine("\"type\": 0,");
                    }
                    if (Childs[i].name.Substring(4, 1) == "2")
                    {
                        sr.WriteLine("\"type\": 3,");
                    }
                    if (Childs[i].name.Substring(4, 1) == "3")
                    {
                        sr.WriteLine("\"type\": 4,");
                    }
                }
                else if (Childs[i].tag == "coin")
                {
                    if (Childs[i].name.Substring(4, 1) == "1")
                    {
                        sr.WriteLine("\"type\": 1,");
                    }
                    if (Childs[i].name.Substring(4, 1) == "2")
                    {
                        sr.WriteLine("\"type\": 2,");
                    }
                }
                else if (Childs[i].tag == "switch")
                {
                    if (Childs[i].name.Substring(6, 1) == "1")
                    {
                        sr.WriteLine("\"type\": 5,");
                    }
                    if (Childs[i].name.Substring(6, 1) == "2")
                    {
                        sr.WriteLine("\"type\": 6,");
                    }
                }
                
                sr.WriteLine("\"x\": "+ Childs[i].transform.position.x +",");
                sr.WriteLine("\"y\": " + Childs[i].transform.position.y + ",");
                sr.WriteLine("\"z\": " + Childs[i].transform.position.z );
                
                if (i == Childs.Count-1) {
                    sr.WriteLine(" }]");
                }
                else
                {
                    sr.WriteLine("}, {");
                }
        }
            sr.Close();
            Debug.Log(fileName + " Updated.");
        }
        else {
            sr = File.AppendText(fileName);
            sr.WriteLine("\"id\": X,");
            sr.WriteLine("\"objects\": [{");

            for (int i = 0; i < Childs.Count; i++)
            {
                if (Childs[i].tag == "trap")
                {
                    if (Childs[i].name.Substring(4, 1) == "1")
                    {
                        sr.WriteLine("\"type\": 0,");
                    }
                    if (Childs[i].name.Substring(4, 1) == "2")
                    {
                        sr.WriteLine("\"type\": 3,");
                    }
                    if (Childs[i].name.Substring(4, 1) == "3")
                    {
                        sr.WriteLine("\"type\": 4,");
                    }
                }
                else if (Childs[i].tag == "coin")
                {
                    if (Childs[i].name.Substring(4, 1) == "1")
                    {
                        sr.WriteLine("\"type\": 1,");
                    }
                    if (Childs[i].name.Substring(4, 1) == "2")
                    {
                        sr.WriteLine("\"type\": 2,");
                    }
                }
                else if (Childs[i].tag == "switch")
                {
                    if (Childs[i].name.Substring(6, 1) == "1")
                    {
                        sr.WriteLine("\"type\": 5,");
                    }
                    if (Childs[i].name.Substring(6, 1) == "2")
                    {
                        sr.WriteLine("\"type\": 6,");
                    }
                }

                sr.WriteLine("\"x\": " + Childs[i].transform.position.x + ",");
                sr.WriteLine("\"y\": " + Childs[i].transform.position.y + ",");
                sr.WriteLine("\"z\": " + Childs[i].transform.position.z);

                if (i == Childs.Count - 1)
                {
                    sr.WriteLine(" }]");
                }
                else
                {
                    sr.WriteLine("}, {");
                }
            }
            sr.Close();
            Debug.Log(fileName + " created.");
        }

        Childs.Clear();
        Traps1.Clear();
        Coins.Clear();
    }

}

