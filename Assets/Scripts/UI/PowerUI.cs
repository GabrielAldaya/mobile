﻿using UnityEngine;
using UnityEngine.UI;

public class PowerUI : MonoBehaviour {

    Text power;
    int prevCounter;
    int prevLives;
    void Awake()
    {
        power = GetComponent<Text>();
        prevCounter = 0;
        prevLives = 0;
    }
    void Start()
    {
        power.text = "power:" + player.Instance.extraLiveCounter;
    }
    void Update()
    {
        if (player.Instance.extraLiveCounter != prevCounter || player.Instance.extraLives != prevLives)
        {
            power.text = "power: " + player.Instance.extraLiveCounter + "%";
            prevCounter = player.Instance.extraLiveCounter;
            prevCounter = player.Instance.extraLives;
        }
    }
}
