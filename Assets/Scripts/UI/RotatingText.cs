﻿using UnityEngine;
using System.Collections;

public class RotatingText : MonoBehaviour {

    public float time;
    public float speed = 0;
    public float min = 0;
    public float max = 0;

    void Start () {

        time = 0;
	}
		
	void Update () {

        time += Time.deltaTime;

        if (time < min)
        {
            transform.Rotate(Vector3.back * (speed * Time.deltaTime), Space.World);
            transform.Rotate(Vector3.right * (speed * Time.deltaTime), Space.World);
        }
        if (time > min && time < max)
        {
            transform.Rotate(Vector3.back * (-speed * Time.deltaTime), Space.World);
            transform.Rotate(Vector3.right * (-speed * Time.deltaTime), Space.World);
        }

        if (time > max)
        {
            time = 0;
        }
    }
}
