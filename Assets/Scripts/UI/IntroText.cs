﻿using UnityEngine;
using System.Collections;

public class IntroText : MonoBehaviour {

    float timer;
    float duration;
    GameObject intro;

	void Start () {
        timer = 0;
        duration = 3f;
        intro = GameObject.Find("Intro");
	}
	
	void Update () {

        timer += Time.deltaTime;

        if (timer > duration)
        {
            intro.SetActive(false);
        }


	}
}
