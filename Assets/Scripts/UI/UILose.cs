﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UILose : MonoBehaviour {
	public Button btnReset;

	void Awake(){
       btnReset.onClick.AddListener (() => Utils.ChangeScene("GRProject"));
	}
}
