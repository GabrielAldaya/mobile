﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIGameplay : MonoBehaviour {
	public Button btnPause;
	public Button btnReset;

	void Awake(){
		btnPause.onClick.AddListener (Main.instance.SwitchPause);
		btnReset.onClick.AddListener (() => Utils.ChangeScene("GRProject"));
	}
}
