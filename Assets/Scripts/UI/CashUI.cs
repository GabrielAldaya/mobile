﻿using UnityEngine;
using UnityEngine.UI;

public class CashUI : MonoBehaviour {
    Text coins;
    int yeah;
    int prev;

	void Awake () {
        coins = GetComponent<Text>();
        coins.text = "YEARS HACKED: 0";
        prev = 0;
	}
	
	void Update () {        

        if (globalVariables.Instance.playerCash != prev)
        {
            coins.text = "YEARS HACKED: " + globalVariables.Instance.playerCash;
            prev = globalVariables.Instance.playerCash;
        }        
	}
}
