﻿using UnityEngine;
using UnityEngine.UI;

public class LivesUI : MonoBehaviour {
    Text coins;
    int yeah;
    int prev;

    void Awake()
    {
        coins = GetComponent<Text>();
        coins.text = "x1";
        prev = 0;
    }

    void Update()
    {

        if (globalVariables.Instance.playerCash != prev)
        {
            coins.text = "x" + player.Instance.extraLives;
            prev = player.Instance.extraLives;
        }
    }
}
