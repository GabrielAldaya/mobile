﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MaxCashUI : MonoBehaviour
{
    Text maxCoins;
    int prev;
    void Awake()
    {
        maxCoins = GetComponent<Text>();        
        prev = 0;
    }
    void Start() 
    {
        maxCoins.text = "MAX HACKED TIME: " + globalVariables.Instance.maxCash;
    }
    void Update()
    {
        if (globalVariables.Instance.maxCash != prev)
        {            
            maxCoins.text = "MAX HACKED TIME: " + globalVariables.Instance.maxCash;
                        
            prev = globalVariables.Instance.maxCash;
        }
    }
}