﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FrameRate : MonoBehaviour {

    float msec;
    float fps;
    float deltaTime = 0.0f;
    Text frames;
    string text;
    float timer;

    void Start () {
        frames = GetComponent<Text>();
        msec = 0;
        fps = 0;
        text = "";
    }
	
	void Update () {
        timer += Time.deltaTime;

        if (timer > 1f)
        {
            FPS();
            timer = 0;
        }
    }

    void FPS()
    {
        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
        msec = deltaTime * 1000.0f;
        fps = 1.0f / deltaTime;
        text = string.Format("FPS: {1:0.}", msec, fps);
        frames.text = text;
    }
}
