﻿using UnityEngine;
using System.Collections;

public class UIManager : MonoBehaviour{
	public static UIManager instance;
	private RectTransform canvas;

	void Awake() {
		instance = this;
		canvas = GameObject.Find("Canvas").GetComponent<RectTransform>();
	}

	public GameObject CreateUI(string name){
		GameObject temp = Instantiate <GameObject>(ResourcesManager.instance.RequestPrefab (name));
        temp.transform.SetParent (canvas,false);

        return temp;
	}
}
