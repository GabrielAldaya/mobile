﻿using UnityEngine;
using System.Collections.Generic;

public class Pool : MonoBehaviour
{
    private Stack<GameObject> objects = new Stack<GameObject>();
    public GameObject prefab;
    public int inicialAmount;


    void Awake(){
       
        Initialize(prefab,inicialAmount);        
    }

    public void Initialize(GameObject prefab, int amount){
		this.prefab = prefab;
		inicialAmount = amount;
	
        for (int i = 0; i < inicialAmount; i++){
            GameObject go = Instantiate(this.prefab);
            go.SetActive(false);

            PoolObject poolObject = go.GetComponent<PoolObject>();
            poolObject.pool = this;

            objects.Push(go);
        }
    }

    public GameObject Get(){
        if (objects.Count == 0){
            GameObject go = Instantiate(prefab);
            go.SetActive(false);

            PoolObject poolObject = go.GetComponent<PoolObject>();
            poolObject.pool = this;

            objects.Push(go);

            return go;
        }
        else { 
            GameObject go = objects.Pop();
            go.SetActive(true);
            return go;
        }
    }

    public void Return(GameObject obj){
        if (obj.activeInHierarchy)
        {
            obj.SetActive(false);
        }
        objects.Push(obj);
    }
}