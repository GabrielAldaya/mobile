﻿using UnityEngine;
using System.Collections;

public class PoolObject : MonoBehaviour
{
    public Pool pool;

    public void Return()
    {        
        pool.Return(this.gameObject);
    }
}