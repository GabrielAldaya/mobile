﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class player : MonoBehaviour
{
    public enum State { IdleState, MovingState };
    public State MyState;
    public int movement;
    public int Cash;
    public int positionx;
    public int positiony;
    public int limitx;
    public int limity;
    public int verticalDirection;
    public bool canRight;
    public bool canLeft;
    public bool canUp;
    public bool rightswipe;
    public bool leftswipe;
    public bool upswipe;
    public bool downswipe;
    public bool rotating;
    public float rotSpeed;
    public float verticalMovSpeed;
    private static player instance = null;
    private Vector3 newPos;
    public float currentRotation;
    public int extraLives;
    int extraLiveCost;
    public int extraLiveCounter;
    public float moveSpeed;
    public bool debuging;

    void Awake()
    {
        currentRotation = transform.localEulerAngles.z;
        MyState = State.IdleState;

        positionx = (int)transform.position.x;
        limitx = 0;
        positiony = (int)transform.position.y;
        limity = 0;
        canUp = true;
        canRight = true;
        canLeft = true;
        Cash = 0;
        rightswipe = false;
        leftswipe = false;
        upswipe = false;
        downswipe = false;
        instance = this;
        rotating = false;
        verticalDirection = 0;
        newPos = new Vector3(0, 0, 0);
        extraLives = 1;
        extraLiveCounter = 0;
        extraLiveCost = 100;
    }

    public static player Instance {
        get {
            return instance;
        }
    }

    public void ChargeExtraLives(int _value)
    {
        extraLiveCounter += _value;
        if (extraLiveCounter >= extraLiveCost )
        {
            extraLives++;
            extraLiveCounter = 0;
        }
    }

    void MoveRight()
    {
        if (MyState != State.MovingState)
        {
            MyState = State.MovingState;
            positionx += 1;
            limitx += 1;
        }
    }

    void MoveLeft()
    {
        if (MyState != State.MovingState)
        {
            MyState = State.MovingState;
            positionx -= 1;
            limitx -= 1;
        }
    }

    void MoveUp()
    {
        if (MyState != State.MovingState)
        {
            MyState = State.MovingState;
            positiony += 2;
            limity += 2;
            verticalDirection = 1;
            rotating = true;
        }
    }

    void MoveDown()
    {
        if (MyState != State.MovingState)
        {
            MyState = State.MovingState;
            positiony -= 2;
            limity -= 2;
            verticalDirection = -1;
            rotating = true;
        }
    }

    void Rotate()
    {
        Vector3 targetRotation = Vector3.zero;
        targetRotation.z = Time.deltaTime * rotSpeed * verticalDirection;
        if (currentRotation >= 180 && verticalDirection>0)
        {
            targetRotation = transform.localEulerAngles;
            targetRotation.z = 0;
            transform.localEulerAngles = targetRotation;
            targetRotation.z = 180;
            currentRotation = 0;
            rotating = false;
        }
        else if(currentRotation <= 0 && verticalDirection<0)
        {
            targetRotation = transform.localEulerAngles;
            currentRotation = 180;
            targetRotation.z = 180;
            transform.localEulerAngles = targetRotation;
            targetRotation.z = -180;
            rotating = false;
        }
        currentRotation += targetRotation.z;
        transform.Rotate(targetRotation);
    }

    void LockMovement()
    {
        if (limitx >= 1)
        {
            canRight = false;
        }
        else
            canRight = true;

        if (limitx <= -1)
        {
            canLeft = false;
        }
        else
            canLeft = true;

        if (limity >= 2)
        {
            canUp = false;
        }
        else
            canUp = true;
    }

    void Movement()
    {
        if (rightswipe == true && canRight == true)
        {
            MoveRight();
        }
        if (leftswipe == true && canLeft == true)
        {
            MoveLeft();
        }

        if (upswipe == true && canUp == true)
        {
            MoveUp();
        }

        if (downswipe == true && canUp == false)
        {
            MoveDown();
        }
    }

    void Update()
    {
		if (Main.instance.paused || !Main.instance.playing) {
			moveSpeed = 0;
		} else {
			moveSpeed = globalVariables.Instance.speed;
		}

		transform.Translate(0, 0, (moveSpeed * Time.deltaTime));

		if (Main.instance.playing)
        {
            Movement();

            if (rotating)
            {
                Rotate();
            }

            if (MyState == State.MovingState)
            {
                newPos.x = positionx;
                newPos.y = positiony;
                newPos.z = transform.position.z;
                transform.position = Vector3.Lerp(transform.position, newPos, Time.deltaTime * verticalMovSpeed);                

                if (rightswipe == true && Mathf.Abs(positionx - transform.position.x) <= 0.2f)
                {
                    MyState = State.IdleState;
                    transform.position = newPos;
                    rightswipe = false;
                }
                if (leftswipe == true && Mathf.Abs(positionx - transform.position.x) <= 0.2f)
                {
                    MyState = State.IdleState;
                    transform.position = newPos;
                    leftswipe = false;
                }
                if (upswipe == true && Mathf.Abs(positiony - transform.position.y) <= 0.2f)
                {
                    MyState = State.IdleState;
                    transform.position = newPos;
                    upswipe = false;
                }
                if (downswipe == true && Mathf.Abs(positiony - transform.position.y) <= 0.2f)
                {
                    MyState = State.IdleState;
                    transform.position = newPos;
                    downswipe = false;
                }
            }


            LockMovement();
        }
    }

    void OnTriggerEnter(Collider col)
        {
        if (!debuging) {
            if (col.gameObject.tag == "trap")
            {
                float distortionMagnitude = Random.Range(0, 2) == 1 ? 0.05f : -0.05f;
                globalVariables.Instance.ChangeDistortionMagnitude(distortionMagnitude);
                if (extraLives <= 0)
                {
                    moveSpeed = 0;
                    Main.instance.EndGame();
                }
            }
            else if (col.gameObject.tag == "switch")
            {
                if (canUp == true)
                {
                    upswipe = true;
                }
                else
                {
                    downswipe = true;
                }
            }
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (!debuging) {
            if (col.gameObject.tag == "trap" && extraLives > 0)
            {
                globalVariables.Instance.speed-=extraLives;
                extraLives--;
            }
        }
    }
}