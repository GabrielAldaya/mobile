﻿using UnityEngine;
using System.Collections;

public class globalVariables : MonoBehaviour {

    public float speed;
    public int pasedChunks;
    public int posicion;
    public int playerCash;
    public int maxCash;
    public bool chunkFlag;
    private float timeCount;

    public GameObject camara;
    public float actualPos;
    private static globalVariables instance= null;

    public Material camMaterial;
    public float distortionDecreaseFactor = 2f;
    float distortion = 0f;

    void Awake () {       
		instance = this;
        chunkFlag = false;
        camara = GameObject.Find("Main Camera");

        posicion = 1;
        actualPos = 0;
        playerCash = 0;
        maxCash = PlayerPrefs.GetInt("highscore",maxCash);
        pasedChunks = 0;
        camMaterial.SetFloat("_Magnitude", 0);
    }
    public static globalVariables Instance {
        get{
            return instance;
        }
    }
    void Update() {
        DecreaseDistorion();
        if (!Main.instance.paused && Main.instance.playing)
        { 
        timeCount += Time.fixedDeltaTime;
        if (timeCount > 1) {
            playerCash += (int)timeCount;
            timeCount = 0;
        }
    }

        if (pasedChunks>10)
        {
            speed += 1;
            pasedChunks = 0;
        }
        StoreHighscore();
	}

    void StoreHighscore()
    {       
        if (playerCash>maxCash)
        {
            maxCash = playerCash;
            PlayerPrefs.SetInt("highscore", maxCash);
        }
    }
    public void ChangeDistortionMagnitude(float amount)
    {
        camMaterial.SetFloat("_Magnitude", amount);
        distortion = amount;
    }

    void DecreaseDistorion()
    {
        if (Mathf.Abs(distortion) > 0.001f)
        {
            distortion = Mathf.Lerp(distortion, 0, distortionDecreaseFactor * Time.deltaTime);//distortion -= distortionDecreaseFactor * Time.deltaTime;
            camMaterial.SetFloat("_Magnitude", distortion);
        }
    }
}


