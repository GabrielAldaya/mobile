﻿using UnityEngine;
using System.Collections;

public class Main : MonoBehaviour {
	public static Main instance;
	public bool playing;
	public bool paused;

	private GameObject uiMenu;
	private GameObject uiGameplay;
	private GameObject uiPaused;
	private GameObject uiLose;

	void Awake() {
		instance = this;

		playing = false;
		paused = false;

		new ResourcesManager();
		ResourcesManager.instance.Initialize ();
	
		new ServicesManager ();

		Utils.CreateEmptyGo (this.transform, "UIManager").AddComponent<UIManager>();
		Utils.CreateEmptyGo (this.transform, "MusicManager").AddComponent<MusicManager>();
        ServicesManager.instance.Activate();
    }

	void Start () {
		uiMenu = UIManager.instance.CreateUI ("Menu");
		MusicManager.instance.PlayMenuSong ();
		globalVariables.Instance.speed = 0;
        ServicesManager.instance.Log();
    }

	public void StartGame(){
		playing = true;
		Destroy (uiMenu);
		uiGameplay =UIManager.instance.CreateUI ("Gameplay");
		uiPaused = GameObject.Find ("PauseMenu");
		uiPaused.SetActive (false);
        MusicManager.instance.PlayRandomSong ();
        globalVariables.Instance.speed = 6;
        ServicesManager.instance.UnlockArchievementTest();
    }

	public void EndGame(){
        playing = false;
		Destroy (uiMenu);
		uiGameplay =UIManager.instance.CreateUI ("Gameplay");

		Destroy (uiGameplay);
		uiLose =UIManager.instance.CreateUI ("Lose");

		MusicManager.instance.musicSource.Stop ();
		ServicesManager.instance.ShowAD ();
	}

	public void SwitchPause(){
		if (paused) {
            ServicesManager.instance.ShowArchievementTest();
            uiPaused.SetActive (false);
            MusicManager.instance.musicSource.UnPause();
		} else {
			if (playing)
			uiPaused.SetActive (true);
            MusicManager.instance.musicSource.Pause();
        }

		paused = !paused;
	}

	public void ReloadScene(){
		Utils.ChangeScene ("GRProject");
	}
}
