﻿using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
#if !UNITY_EDITOR
using UnityEngine.Advertisements;
#endif

public class ServicesManager
{
    public static ServicesManager instance;

	public ServicesManager(){
		instance = this;
#if !UNITY_EDITOR
        Advertisement.Initialize("1642896");
#endif
    }

    public void ShowAD(){
#if !UNITY_EDITOR
        Advertisement.Show();
#endif
    }

    public void UnlockArchievementTest()
    {
        // unlock achievement (achievement ID "CgkIieX2gZ4MEAIQAg ")
        Social.ReportProgress(GPGSIds.achievement_welcome, 200.0f, (bool success) => {});
    }

    public void Log()
    {
        if (!PlayGamesPlatform.Instance.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.Authenticate((bool success) => { });
        }
    }

    public void ShowArchievementTest()
    {
        Social.ShowAchievementsUI();
    }

    public void Activate()
    {
        PlayGamesClientConfiguration config = new
             PlayGamesClientConfiguration.Builder()
             .Build();

        // Initialize and activate the platform
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();
    }
}
