﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Utils {
	
	public static GameObject CreateEmptyGo(Transform parent, string name){
		GameObject temp = new GameObject (name);
		temp.transform.SetParent (parent);

		return temp;
	}

	public static void ChangeScene(string name){

		SceneManager.LoadScene (name);
	}
}
