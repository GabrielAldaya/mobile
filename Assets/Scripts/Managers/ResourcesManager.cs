﻿using UnityEngine;
using System.Collections;

public class ResourcesManager {
	public static ResourcesManager instance;
	private AudioClip[] songArray;
	private GameObject[] prefabArrays;

	public ResourcesManager(){
		instance = this;
	}

	public void Initialize(){
		songArray = Resources.LoadAll<AudioClip>("Audio/Music");
		prefabArrays = Resources.LoadAll<GameObject>("Prefabs");
	}

	public AudioClip RequestAudioClip(string clip){
		for (int i = 0; i < songArray.Length; i++) {
			if (songArray [i].name == clip) {
				return songArray [i];
			}
		}
		return null;
	}

	public AudioClip RequestAudioClip(int index){
		if (songArray [index] != null) {
			return songArray [index];
		}

		return null;
	}

	public GameObject RequestPrefab(string name){
		for (int i = 0; i < prefabArrays.Length; i++) {
			if (prefabArrays [i].name == name) {
				return prefabArrays [i];
			}
		}

		return null;
	}

	public int SongsArrayLenght(){
		return songArray.Length;
	}
		
}
