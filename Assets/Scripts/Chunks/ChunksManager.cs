﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChunksManager : MonoBehaviour {

    public float pos;
    public int rnd;
    public int prev;
    public bool firstChunk;
    public PoolObject pool;
    public List<GameObject> traps;
    private ReadJson readJson;    
    private GameObject floor1;
    private GameObject floor2;
    private GameObject floor3;
    private GameObject floor4;
    private GameObject floor5;
    GameObject temp = null;
    int num;

    void Awake()
    {
        readJson = GameObject.Find("goGlobal").GetComponent<ReadJson>();       
        floor1 = GameObject.Find("Piso1");
        floor2 = GameObject.Find("Piso2");
        floor3 = GameObject.Find("Piso3");
        floor4 = GameObject.Find("Piso4");
        floor5 = GameObject.Find("Piso5");
    }

    void Start () {
        pos = 100;
        firstChunk = true;
        prev = 0;               
        InitLevel();
	}

	void Update () {

        if (globalVariables.Instance.chunkFlag == true)
        {
            DestroyChunk();
            switch (globalVariables.Instance.posicion)
            {
                case 1:
                    floor1.transform.Translate(0, 0, pos);
                    globalVariables.Instance.posicion++;
                    CreateChunk();                   
                    break;

                case 2:

                    floor2.transform.Translate(0, 0, pos);
                    globalVariables.Instance.posicion++;
                    CreateChunk();                   
                    break;

                case 3:

                    floor3.transform.Translate(0, 0, pos);
                    globalVariables.Instance.posicion++;
                    CreateChunk();
                    break;

                case 4:

                    floor4.transform.Translate(0, 0, pos);
                    globalVariables.Instance.posicion++;
                    CreateChunk();
                    break;

                case 5:

                    floor5.transform.Translate(0, 0, pos);
                    globalVariables.Instance.posicion = 1;
                    CreateChunk();
                    break;
            }
            globalVariables.Instance.chunkFlag = false;
        }        	    
	}

    void InitLevel()
    {
        for (int i = 0; i < 3; i++)
        {
            CreateChunk();
        }
    }

    public void DestroyChunk()
    {
        if (firstChunk == true)
        {
            firstChunk = false;
        }
        else {
            globalVariables.Instance.pasedChunks += 1;
            temp = null;
            temp = traps[0];
            if (temp != null)
                for (int i = 0; i < temp.transform.childCount; i++)
                {
                    pool = temp.transform.GetChild(i).GetComponent("PoolObject") as PoolObject;
                    pool.Return();
                }
            traps.RemoveAt(0);
            pool = temp.GetComponent("PoolObject") as PoolObject;
            pool.Return();
        }
    }

    public void CreateChunk(){
        GameObject chunk = null;
        do{
            rnd = Random.Range(0, 12);
        } while (rnd == prev);
        prev = rnd;
        chunk = readJson.spawnChunk(readJson.GetItem(rnd));
        chunk.transform.Translate(0, 0, globalVariables.Instance.actualPos);
        globalVariables.Instance.actualPos += 20;
        num++;
        chunk.name = "chunk" + num;
        traps.Add(chunk);
    }
}
