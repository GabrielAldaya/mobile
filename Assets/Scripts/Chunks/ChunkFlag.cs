﻿using UnityEngine;
using System.Collections;

public class ChunkFlag : MonoBehaviour {
    public bool Flag=false;
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            globalVariables.Instance.chunkFlag = true;
            globalVariables.Instance.pasedChunks += 1;
        }
    }
}
