﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using LitJson;

public class ReadJson : MonoBehaviour
{
    private string jsonString;
    private JsonData chunkData;
    public Pool poolTrap1;
    public Pool poolTrap2;
    public Pool poolTrap3;
    public Pool poolSwitch1;
    public Pool poolSwitch2;
    public Pool poolCoins1;
    public Pool poolCoins2;
    public Pool poolParents;
    public GameObject[] trapsPrefabs;
    public GameObject PrefabParent;
    public GameObject[] coinsPrefabs;
    public GameObject Trap1;
    public GameObject Trap2;
    public GameObject Trap3;
    public GameObject Switch1;
    public GameObject Switch2;
    public GameObject PrefabCoin;
    public GameObject PrefabCoin2;
    public bool trap;
    public int Traps1Amount;
    public int Traps2Amount;
    public int Traps3Amount;
    public int Switch1Amount;
    public int Switch2Amount;
    public int ChunksAmount;
    public int CoinsAmount;
    public int Coins2Amount;
    public TextAsset mytxtData;
    GameObject parent = null;
    GameObject temp = null;
    GameObject go = null;

    void Awake()
    {
        Trap1 = Resources.Load<GameObject>("Prefabs/Gameplay/Traps/trap");
        Trap2 = Resources.Load<GameObject>("Prefabs/Gameplay/Traps/trap2");
        Trap3 = Resources.Load<GameObject>("Prefabs/Gameplay/Traps/trap3");
        Switch1 = Resources.Load<GameObject>("Prefabs/Gameplay/Traps/switch");
        Switch2 = Resources.Load<GameObject>("Prefabs/Gameplay/Traps/switch2");
        PrefabCoin = Resources.Load<GameObject>("Prefabs/Gameplay/Coins/coin");
        PrefabCoin2 = Resources.Load<GameObject>("Prefabs/Gameplay/Coins/coin2");
        PrefabParent = Resources.Load<GameObject>("Prefabs/Gameplay/Parents/parent");

        poolTrap1 = gameObject.AddComponent<Pool>();
        poolTrap2 = gameObject.AddComponent<Pool>();
        poolTrap3 = gameObject.AddComponent<Pool>();
        poolSwitch1 = gameObject.AddComponent<Pool>();
        poolSwitch2 = gameObject.AddComponent<Pool>();
        poolCoins1 = gameObject.AddComponent<Pool>();
        poolCoins2 = gameObject.AddComponent<Pool>();
        poolParents = gameObject.AddComponent<Pool>();
        
        mytxtData = Resources.Load("Jsons/chunk") as TextAsset;
        jsonString = mytxtData.ToString();
        chunkData = JsonMapper.ToObject(jsonString);
    }
    void Start()
    {
        Traps1Amount = 40;
        Traps2Amount = 10;
        Traps3Amount = 15;
        CoinsAmount = 35;
        Coins2Amount = 15;
        ChunksAmount = 5;
        Switch1Amount = 10;
        Switch2Amount = 10;
        poolTrap1.Initialize(Trap1, Traps1Amount);
        poolTrap2.Initialize(Trap2, Traps2Amount);
        poolTrap3.Initialize(Trap3, Traps3Amount);
        poolSwitch1.Initialize(Switch1, Switch1Amount);
        poolSwitch2.Initialize(Switch2, Switch2Amount);
        poolCoins1.Initialize(PrefabCoin, CoinsAmount);
        poolCoins2.Initialize(PrefabCoin2, Coins2Amount);
        poolParents.Initialize(PrefabParent, ChunksAmount);
    }

    public JsonData GetItem(int chunkID)
    {
        for (int i = 0; i < chunkData["chunks"].Count; i++)
        {
            if ((int)chunkData["chunks"][i]["id"] == chunkID)
                return chunkData["chunks"][i]["objects"];
        }
        return chunkID;
    }

    public GameObject spawnChunk(JsonData objects)
    {
        parent = poolParents.Get();       
        temp = null;
        go=null;
        
        for (int i = 0; i < objects.Count; i++)
        {
            switch ((int)objects[i]["type"])
            {
                case 0:
                go = poolTrap1.Get();
                trap = true;
                    break;
                case 1:
                temp = poolCoins1.Get();
                    trap = false;
                    break;
                case 2:
                    temp = poolCoins2.Get();
                    trap = false;
                    break;
                case 3:
                    go = poolTrap2.Get();
                    trap = true;
                    break;
                case 4:
                    go = poolTrap3.Get();
                    trap = true;
                    break;
                case 5:
                    go = poolSwitch1.Get();;
                    trap = true;
                    break;
                case 6:
                    go = poolSwitch2.Get();
                    trap = true;
                    break;
            }

            Vector3 pos = new Vector3((int)objects[i]["x"], (int)objects[i]["y"], (int)objects[i]["z"]);

            if (!trap){
                temp.transform.position = (pos);
                temp.transform.SetParent(parent.transform);
                temp.GetComponent<Collider>().enabled = true;
            }
            else{
                go.transform.position = pos;
                go.transform.SetParent(parent.transform);
                go.GetComponent<Collider>().enabled = true;
            }           
        }        
        trap = false;
        return parent;
    }    
}
