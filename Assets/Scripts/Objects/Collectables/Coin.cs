﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour {

    public int value;

    void OnTriggerEnter(Collider col) {
        if (col.tag == "Player") {
            if (player.Instance.extraLives >= 3)
            {
                globalVariables.Instance.playerCash += value;
            }
            else
            {
                player.Instance.ChargeExtraLives(value);
            }
            this.gameObject.SetActive(false);
        }
    }
}
