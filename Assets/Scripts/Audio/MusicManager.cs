﻿using UnityEngine;
//using System.IO;
using System.Collections;

public class MusicManager : MonoBehaviour {
	public static MusicManager instance;

	int lastSongIndex;

    public AudioSource musicSource;

    void Awake () {

		instance = this;

		musicSource = gameObject.AddComponent<AudioSource> ();
		musicSource.loop = false;
		musicSource.playOnAwake = true;
    }

    void Start() {
        lastSongIndex = -1;
    }

	public void PlayMenuSong() {
        musicSource.loop = true;
        musicSource.clip = ResourcesManager.instance.RequestAudioClip (0);
		musicSource.Play();
	}

	public void PlayRandomSong() {
        musicSource.loop = false;
        PickSong();
		lastSongIndex = PickSong();
		musicSource.clip = ResourcesManager.instance.RequestAudioClip (lastSongIndex);
        musicSource.Play();
    }

	void NextSong () {
		if (!musicSource.isPlaying && !Main.instance.paused && Main.instance.playing) {
			PlayRandomSong();
		}
	}

	int PickSong() {
		int rnd;

		do { 
			rnd = Random.Range(1, ResourcesManager.instance.SongsArrayLenght());
		} while (lastSongIndex == rnd);

		return rnd;
	}
    private void Update()
    {
        NextSong();
    }
}
