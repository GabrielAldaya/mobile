﻿using UnityEngine;

public class SwipeDetector : MonoBehaviour
{
    public float minSwipeDistY = 10;
    public float minSwipeDistX = 5;
    public Vector2 touchPos;

    private Vector2 startPos;
    private Vector2 endPos;
    private Vector2 diffPos;
    private Vector3 touchX;
    private Vector3 startingPosX;
    private Vector3 touchY;
    private Vector3 startingPosY;
    private bool swiping;

    void Start() {
        touchX = new Vector3(0, 0, 0);
        touchY = new Vector3(0, 0, 0);
        startingPosX = new Vector3(0, 0, 0);
        startingPosY = new Vector3(0, 0, 0);
        swiping = false;
    }
    void Update()
    {
#if UNITY_EDITOR

        if (!Main.instance.paused)
        {
            if (Input.GetKeyDown("d") && player.Instance.canRight)
            {
                player.Instance.rightswipe = true;
            }
            if (Input.GetKeyDown("a") && player.Instance.canLeft)
            {
                player.Instance.leftswipe = true;
            }

            if (Input.GetKeyDown("w") && player.Instance.canUp)
            {
                player.Instance.upswipe = true;
            }

            if (Input.GetKeyDown("s") && !player.Instance.canUp)
            {
                player.Instance.downswipe = true;
            }
        }
#endif


#if UNITY_ANDROID
        if (Input.touchCount > 0)
        {
            Touch touch = Input.touches[0];
            if (!Main.instance.paused)
            {
                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        startPos = touch.position;
                        break;

                    case TouchPhase.Moved:
                        endPos = touch.position;
                        diffPos = endPos - startPos;
                        diffPos.Normalize();
                        touchX.x = touch.position.x;
                        touchY.y = touch.position.y;
                        startingPosX.x = startPos.x;
                        startingPosY.y = startPos.y;
                        float swipeDistHorizontal = (touchX - startingPosX).magnitude;
                        float swipeDistVertical = (touchY - startingPosY).magnitude;

                        if (swipeDistVertical > minSwipeDistY)
                        {
                            float swipeValue = Mathf.Sign(touch.position.y - startPos.y);

                            if (swipeValue > 0)//up swipe
                            {
                                if (diffPos.y > 0 && diffPos.x > -0.5f && diffPos.x < 0.5f && player.Instance.canUp)
                                {
                                    player.Instance.upswipe = true;
                                    startPos = touch.position;
                                }
                            }

                            else if (swipeValue < 0)//down swipe
                            {
                                if (diffPos.y < 0 && diffPos.x > -0.5f && diffPos.x < 0.5f && !player.Instance.canUp)
                                {
                                    player.Instance.downswipe = true;
                                    startPos = touch.position;
                                }
                            }
                        }

                        if (swipeDistHorizontal > minSwipeDistX)
                        {
                            float swipeValue = Mathf.Sign(touch.position.x - startPos.x);

                            if (swipeValue > 0)//right swipe
                            {
                                if (diffPos.x > 0 && diffPos.y > -0.5f && diffPos.y < 0.5f && player.Instance.canRight)
                                {
                                    player.Instance.rightswipe = true;
                                    startPos = touch.position;
                                }
                            }
                            else if (swipeValue < 0)//left swipe
                            {
                                if (diffPos.x < 0 && diffPos.y > -0.5f && diffPos.y < 0.5f && player.Instance.canLeft)
                                {
                                    player.Instance.leftswipe = true;
                                    startPos = touch.position;
                                }
                            }
                        }
                        break;

                    case TouchPhase.Stationary:
                        player.Instance.leftswipe = false;
                        player.Instance.rightswipe = false;
                        player.Instance.upswipe = false;
                        player.Instance.downswipe = false;
                        break;
                }
            }
        }
#endif
    }
}
