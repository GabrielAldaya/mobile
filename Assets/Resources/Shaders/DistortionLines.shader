﻿Shader "GEAK_Shaders/DistortionLines"
{
	Properties
	{
		_MainTex("Texture", 2D) = "black" {}
	//Distortion Texture
	_DistTex("Distortion Texture", 2D) = "grey" {}
	//Speed of Displacement
	_XDisplacement("XDisplacement",Float) = 0
		_YDisplacement("YDisplacement",Float) = 0.5
		//Initial Position of Texture
		_XPos("XPos",Float) = 0
		_YPos("YPos",Float) = 0
		//Multiplier of the Distortion Texture
		_Magnitude("Magnitude",Range(-0.1,0.1)) = 2
		//Amount of Distortion Lines Repetition
		_Repetition("Repetition",int) = 2
		//Distance between Distortion Lines
		_LineDistance("Line Distance",Float) = 0.2
		//Multiplier that eneables Line Bounce
		_BounceEneable("Bounce Eneable",Range(0,1)) = 1
		//Multiplier of Time that increases the Bounce Speed
		_BounceSpeed("Bounce Speed",int) = 10
		//Divider of the Bounce that limits the distance of the bounce
		_BounceLimiter("Bounce Limit Divider",int) = 20
	}
		SubShader
	{
		Tags{ "RenderType" = "Opaque" }

		Pass
	{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"

		struct appdata
	{
		float4 vertex : POSITION;
		float2 uv : TEXCOORD0;
	};

	struct v2f
	{
		float2 uv : TEXCOORD0;
		float4 vertex : SV_POSITION;
	};

	sampler2D _MainTex, _DistTex;
	float _XDisplacement , _YDisplacement;
	float _XPos;
	float _YPos;
	float _Magnitude;
	float _LineDistance;
	int _Repetition;
	int _BounceSpeed, _BounceLimiter,_BounceEneable;

	v2f vert(appdata v)
	{
		v2f o;
		o.vertex = UnityObjectToClipPos(v.vertex);
		o.uv = v.uv;
		return o;
	}

	fixed4 frag(v2f i) : SV_Target
	{
		float2 Pos = float2(_XPos,_YPos);
		float2 distScroll = float2(_Time.x*_XDisplacement,_Time.x* _YDisplacement);

		float bounce = (sin(_Time.x*_BounceSpeed) / _BounceLimiter) * _BounceEneable;

		float2 text2 = tex2D(_DistTex, i.uv + (Pos + distScroll));

		for (int j = 0; j < _Repetition; j++) {
			text2 = tex2D(_DistTex, i.uv + ((_LineDistance + _LineDistance * j) + bounce*(j + 1)) + (Pos + distScroll)) + text2;
		}

		text2 = text2 * _Magnitude;
		float4 col = tex2D(_MainTex, i.uv + text2);
		return col;

	}
		ENDCG
	}
	}
}

